from conans import ConanFile, CMake, tools
import shutil

class LibxlsxwriterConan(ConanFile):
    name            = "libxlsxwriter"
    license         = "FreeBSD license"
    author          = "toge.mail@gmail.com"
    url             = "https://bitbucket.org/toge/conan-libxlsxwriter/"
    homepage        = "https://github.com/jmcnamara/libxlsxwriter/"
    description     = "A C library for creating Excel XLSX files. https://libxlsxwriter.github.io"
    topics          = ("Excel", "xlsx", "create", "write")
    settings        = "os", "compiler", "build_type", "arch"
    generators      = "cmake"
    requires        = ["minizip/[>= 1.2.11]"]
    exports_sources  = ["CMakeLists.txt"]
    options         = {
        "shared"             : [True, False],
        "standard_tmpfile"   : {True, False},
        "no_md5"             : {True, False},
        "fmemopen"           : {True, False},
        "ioapi_no_64"        : {True, False},
        "static_msvc_runtime": {True, False}
    }
    default_options = {
        "shared"             : False,
        "standard_tmpfile"   : False,
        "no_md5"             : False,
        "fmemopen"           : False,
        "ioapi_no_64"        : False,
        "static_msvc_runtime": False
    }

    @property
    def _source_subfolder(self):
        return "source_subfolder"

    def source(self):
        tools.get(**self.conan_data["sources"][self.version])
        shutil.move("libxlsxwriter-RELEASE_" + self.version, self._source_subfolder)

        tools.replace_in_file(self._source_subfolder + "/cmake/FindMINIZIP.cmake", "if(MINIZIP_FOUND)", "if(0)")

    def build(self):
        cmake = CMake(self)
        cmake.definitions["BUILD_SHARED_LIBS"]       = self.options.shared
        cmake.definitions["BUILD_TESTS"]             = False
        cmake.definitions["BUILD_EXAMPLES"]          = False
        cmake.definitions["USE_SYSTEM_MINIZIP"]      = True
        cmake.definitions["USE_STANDARD_TMPFILE"]    = self.options.standard_tmpfile
        cmake.definitions["USE_NO_MD5"]              = self.options.no_md5
        cmake.definitions["USE_FMEMOPEN"]            = self.options.fmemopen
        cmake.definitions["IOAPI_NO_64"]             = self.options.ioapi_no_64
        cmake.definitions["USE_STATIC_MSVC_RUNTIME"] = self.options.static_msvc_runtime
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy("*.h"    , dst = "include", src       = self._source_subfolder + "/include")
        self.copy("*.lib"  , dst = "lib"    , keep_path = False)
        self.copy("*.dll"  , dst = "bin"    , keep_path = False)
        self.copy("*.so"   , dst = "lib"    , keep_path = False)
        self.copy("*.dylib", dst = "lib"    , keep_path = False)
        self.copy("*.a"    , dst = "lib"    , keep_path = False)
        self.copy("License.txt", dst="licenses", src=self._source_subfolder)

    def package_info(self):
        self.cpp_info.libs = ["xlsxwriter"]
